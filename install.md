# Installation
This guide wil show you how to make turn Arch Linux installation into a Manjaro installation.

# Cleanup
run the following command as root:
```bash
pacman -Syy && rm /var/lib/pacman/sync/*db
```

# Replace Arch Linux's os-release file with Manjaro's os-release file
This step is required for pacman updates as it sometimes does check your os-release. This also tricks neofetch and it will show that you are indeed using Manjaro. This can be done by editing your os-release file.

Run the following command to remove your existing os-release file. (run this command as root)
```bash
rm /etc/os-release
```

Now add the Manjaro os-release file. (File included in our repo). Clone into this repo to access the os-release file.
Once you have done that run this command as root:
```bash
cp ./os-release /etc/os-release
```

# Using the Manjaro repos instead of the Arch Repos.
Simmilar to the os-release file we need to chane the pacman.conf file to use Manjaro repos. We have already done this for you. The pacman.conf file we provide is customized to give you better speeds and is compatible with the Manjaro stable repos.

run this command as root to remove your existing pacman config, and replace it with the pacman config.
```bash
rm /etc/pacman.conf && cp ./pacman.conf /etc/pacman.conf
```

Once all of this is done reboot and run this command
```bash
sudo pacman -Syy
```

That's it your all done.
